# Librus API

## Usage

```python
from librus import LibrusSession 
session = LibrusSession()
session.login("1234567u", "p4ssw0rD")
for announcement in session.list_announcements():
    print(announcement.title)
```

## API completeness
- [x] Grades
- [x] Absences
- [ ] Timetable
- [x] Exams
- [x] Announcements
- [ ] Messages
- - [x] listing/reading
- - [ ] sending
- [ ] Homework
- [ ] Teacher notes
- [ ] Lucky number, if available
